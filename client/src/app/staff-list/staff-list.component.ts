import { Component, OnInit } from '@angular/core';
import { StaffService } from '../shared/staff/staff.service';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.css']
})
export class StaffListComponent implements OnInit {

  staffs: Array<any>;

  constructor(private staffService: StaffService) { }

  ngOnInit() {
    this.staffService.getAll().subscribe(data => {
      this.staffs = data;
    });
  }

}
