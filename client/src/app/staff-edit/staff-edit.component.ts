import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from '../shared/staff/staff.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-staff-edit',
  templateUrl: './staff-edit.component.html',
  styleUrls: ['./staff-edit.component.css']
})
export class StaffEditComponent implements OnInit, OnDestroy {
  staff: any = {};

  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private staffService: StaffService) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.staffService.get(id).subscribe((staff: any) => {
          if (staff) {
            this.staff = staff;
            this.staff.href = staff._links.self.href;
          } else {
            console.log(`Staff with id '${id}' not found, returning to list`);
            this.gotoList();
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/staff-list']);
  }

  save(form: NgForm) {
    this.staffService.save(form).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

  remove(href) {
    this.staffService.remove(href).subscribe(result => {
      this.gotoList();
    }, error => console.error(error));
  }

}
