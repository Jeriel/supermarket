package br.edu.ifsp.bra.supermarket.supermarket.services;

import br.edu.ifsp.bra.supermarket.supermarket.domain.Staff;

import java.util.List;

public interface StaffService {

    List<Staff> listAll();

    Staff getById(String id);

    Staff insert(Staff staff);

    Staff update(Staff staff);

    void delete(String id);

}
