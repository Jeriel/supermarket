package br.edu.ifsp.bra.supermarket.supermarket.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @RequestMapping("/")
    @CrossOrigin(origins = "http://localhost:4200")
    String index() {
        return "index";
    }
}
