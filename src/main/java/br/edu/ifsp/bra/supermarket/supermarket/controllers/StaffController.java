package br.edu.ifsp.bra.supermarket.supermarket.controllers;

import br.edu.ifsp.bra.supermarket.supermarket.domain.Staff;
import br.edu.ifsp.bra.supermarket.supermarket.services.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/staff")
public class StaffController {

    private StaffService staffService;

    @Autowired
    public StaffController(StaffService staffService) {
        this.staffService = staffService;
    }

    @GetMapping("/list")
    public Collection<Staff> listStaffs() {
        return staffService.listAll()
                .stream()
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Staff getStaff(@PathVariable String id) {
       return staffService.getById(id);
    }

    @PostMapping("/")
    public void insert(@RequestBody Staff staff) {
      staffService.insert(staff);
    }

    @PutMapping("/")
    public void update(@RequestBody Staff staff) {
      staffService.update(staff);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        staffService.delete(id);
    }
}
