package br.edu.ifsp.bra.supermarket.supermarket;

import br.edu.ifsp.bra.supermarket.supermarket.domain.Staff;
import br.edu.ifsp.bra.supermarket.supermarket.repositories.StaffRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class SupermarketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupermarketApplication.class, args);
	}

	@Bean
	ApplicationRunner init(StaffRepository repository) {
		return args -> {
			Stream.of("Atendente", "Gerente", "Açogueiro").forEach(name -> {
				var staff = new Staff();
				staff.setName(name);
				repository.save(staff);
			});
			repository.findAll().forEach(System.out::println);
		};
	}

}
