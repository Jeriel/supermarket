package br.edu.ifsp.bra.supermarket.supermarket.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Document(collection = "Staffs")
@Getter @Setter @NoArgsConstructor
public class Staff {
    @Id
    private String id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String password;

    @NotNull
    private boolean admin;
}
