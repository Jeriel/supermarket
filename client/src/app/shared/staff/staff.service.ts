import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  public API = '//localhost:8080';
  public STAFF_API = this.API + '/staff';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.STAFF_API + '/list');
  }

  get(id: string) {
    return this.http.get(this.STAFF_API + '/' + id);
  }

  save(staff: any): Observable<any> {
    let result: Observable<Object>;
    if (staff['href']) {
      result = this.http.put(staff.href, staff);
    } else {
      result = this.http.post(this.STAFF_API, staff);
    }
    return result;
  }

  remove(href: string) {
    return this.http.delete(href);
  }
}
