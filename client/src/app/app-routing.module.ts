import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffListComponent } from './staff-list/staff-list.component';
import { StaffEditComponent } from './staff-edit/staff-edit.component';

const routes: Routes = [
  { path: '', redirectTo: '/staff-list', pathMatch: 'full' },
  {
    path: 'staff-list',
    component: StaffListComponent
  },
  {
    path: 'staff-add',
    component: StaffEditComponent
  },
  {
    path: 'staff-edit/:id',
    component: StaffEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
