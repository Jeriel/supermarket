package br.edu.ifsp.bra.supermarket.supermarket.repositories;

import br.edu.ifsp.bra.supermarket.supermarket.domain.Staff;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin(origins = "http://localhost:4200")
public interface StaffRepository extends MongoRepository<Staff, String> {
    Staff findByName(String name);
}
