package br.edu.ifsp.bra.supermarket.supermarket.services;

import br.edu.ifsp.bra.supermarket.supermarket.domain.Staff;
import br.edu.ifsp.bra.supermarket.supermarket.repositories.StaffRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {

    private StaffRepository repository;

    @Autowired
    public StaffServiceImpl(StaffRepository repository) {
        this.repository = repository;
    }


    @Override
    public List<Staff> listAll() {

        List<Staff> staffs = new ArrayList<>();

        repository.findAll().forEach(staffs::add);

        return staffs;
    }

    @Override
    public Staff getById(String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public Staff insert(Staff staff) {
        return repository.insert(staff);
    }

    @Override
    public Staff update(Staff staff) { return repository.save(staff); }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}
