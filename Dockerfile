FROM openjdk:11
VOLUME /tmp
ADD build/libs/supermarket-0.0.1.jar supermarket.jar
EXPOSE 8085
RUN sh -c 'touch /supermarket.jar'
ENTRYPOINT ["java", "-Dspring.data.mongodb.uri=mongodb://mongocontainer/supermarket", "-Djava.security.egd=file:/dev/.urandom", "-jar", "supermarket.jar"]
